/*
 * vcnl4020.h
 *
 *  Created on: May 4, 2020
 *      Author: lucad
 */

#ifndef INC_VCNL4020_H_
#define INC_VCNL4020_H_



#include "main.h"


#define VCNL4020_ADDRESS					  0x26					 //I2C master address

#define VCNL4020_COMMAND_REG                  0x80                   //Starts measurements, relays data ready info
#define VCNL4020_PRODUCT_ID_REVISION_ID_REG   0x81                   //Product ID/revision ID, should read 0x21
#define VCNL4020_PROXIMITY_RATE_REG           0x82                   //Proximity Rate Measurement 000-111 1.95-250 measurement/s
#define VCNL4020_IR_LED_CURRENT_REG           0x83                   //Sets IR LED current in steps of 10mA 0-200mA 0x00-0x14
#define VCNL4020_AMBIENT_PARAMETER_REG        0x84                   //Configures ambient light measures
#define VCNL4020_AMBIENT_RESULT_MSB_REG       0x85                   //Most significant byte of ambient light measure
#define VCNL4020_AMBIENT_RESULT_LSB_REG       0x86                   //Least significant byte of ambient light measure
#define VCNL4020_PROXIMITY_RESULT_MSB_REG     0x87                   //Most significant byte of proximity measure
#define VCNL4020_PROXIMITY_RESULT_LSB_REG     0x88                   //Least significant byte of proximity measure
#define VCNL4020_INTERRUPT_CONTROL_REG        0x89                   //Configures interrupt
#define VCNL4020_LOW_THRESHOLD_MSB_REG        0x8A                   //Most significant byte of low threshold
#define VCNL4020_LOW_THRESHOLD_LSB_REG        0x8B                   //Least significant byte of low threshold
#define VCNL4020_HIGH_THRESHOLD_MSB_REG       0x8C                   //Most significant byte of high threshold
#define VCNL4020_HIGH_THRESHOLD_LSB_REG       0x8D                   //Least significant byte of high threshold
#define VCNL4020_INTERRUPT_STATUS_REG         0x8E                   //Read and clear interrupt status

void I2C_Write_Byte(I2C_HandleTypeDef *hi2c, uint8_t address, uint8_t reg, uint8_t data);
uint8_t I2C_Read_Byte(I2C_HandleTypeDef *hi2c, uint8_t address, uint8_t reg);
void VCNL4020_Init(I2C_HandleTypeDef *hi2c, uint16_t high_threshold);
void VCNL4020_Set_Threshold(I2C_HandleTypeDef *hi2c, uint16_t low, uint16_t high);


#endif /* INC_VCNL4020_H_ */
