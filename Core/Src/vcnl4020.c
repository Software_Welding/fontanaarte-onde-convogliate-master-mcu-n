/*
 * vcnl4020.c
 *
 *  Created on: May 4, 2020
 *      Author: lucad
 */

#include "main.h"
#include "vcnl4020.h"

void I2C_Write_Byte(I2C_HandleTypeDef *hi2c, uint8_t address, uint8_t reg, uint8_t data)
{
	uint8_t Buffer[2] = {reg, data};
	HAL_I2C_Master_Transmit(hi2c, address, Buffer, 2U, 500);
}


uint8_t I2C_Read_Byte(I2C_HandleTypeDef *hi2c, uint8_t address, uint8_t reg)
{
	uint8_t data;
	HAL_I2C_Master_Transmit(hi2c, address, &reg, 1U, 500);
	HAL_I2C_Master_Receive(hi2c, address, &data, 1U, 500);
	return data;
}

void VCNL4020_Set_Threshold(I2C_HandleTypeDef *hi2c, uint16_t low, uint16_t high)
{
	uint8_t MSB_low = (low & 0xFF00) >>8;
	uint8_t LSB_low = low & 0x00FF;


	I2C_Write_Byte(hi2c, VCNL4020_ADDRESS, VCNL4020_LOW_THRESHOLD_MSB_REG, MSB_low);
	I2C_Write_Byte(hi2c, VCNL4020_ADDRESS, VCNL4020_LOW_THRESHOLD_LSB_REG, LSB_low);

	uint8_t MSB_high = (high & 0xFF00) >>8;
	uint8_t LSB_high = high & 0x00FF;

	I2C_Write_Byte(hi2c, VCNL4020_ADDRESS, VCNL4020_HIGH_THRESHOLD_MSB_REG, MSB_high);
	I2C_Write_Byte(hi2c, VCNL4020_ADDRESS, VCNL4020_HIGH_THRESHOLD_LSB_REG, LSB_high);
}

void VCNL4020_Init(I2C_HandleTypeDef *hi2c, uint16_t high_threshold)
{
	I2C_Write_Byte(hi2c, VCNL4020_ADDRESS, VCNL4020_IR_LED_CURRENT_REG, 0x14);

	I2C_Write_Byte(hi2c, VCNL4020_ADDRESS, VCNL4020_PROXIMITY_RATE_REG, 0x07);

	I2C_Write_Byte(hi2c, VCNL4020_ADDRESS, VCNL4020_INTERRUPT_CONTROL_REG, 0x02);

	VCNL4020_Set_Threshold(hi2c, 0, high_threshold);

	I2C_Write_Byte(hi2c, VCNL4020_ADDRESS, VCNL4020_COMMAND_REG, 0x03);
}


