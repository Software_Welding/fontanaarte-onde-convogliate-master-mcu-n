/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "vcnl4020.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define DIMMING_CH1_FLASH 				0x0800F800						//Reference Manual -> 3 Embedded Flash memory (FLASH) -> Flash memory organization table
#define DIMMING_CH2_FLASH 				0x0800F810
#define POWER_FLASH      				0x0800F820
#define DIMMINGDIRECTION_CH1_FLASH      0x0800F830
#define DIMMINGDIRECTION_CH2_FLASH		0x0800F840
#define CHANNEL_SELECTED_FLASH			0x0800F850



//#define PROXIMITY
//#define TOUCH

#define SLAVE_RESPONSE
#define ONE_CHANNEL_OFF



#define MIN_PWM				   					 1
#define MAX_PWM								   253

#define MINIMUM_SHORT_TOUCH_TIME                10
#define MAXIMUM_SHORT_TOUCH_TIME			   400
#define MAXIMUM_DOUBLE_TOUCH_TIME			   500

#define MANUAL_RAMP_DELAY_TIME                   2		//mSec
#define AUTOMATIC_RAMP_DELAY_TIME             3000      //Cycle

#define THRESHOLD_LEVEL						18000 //18000		//4000 senza copertura
#define MIN_LED_BLUE_LEVEL						50

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
COMP_HandleTypeDef hcomp1;

I2C_HandleTypeDef hi2c2;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim14;
TIM_HandleTypeDef htim17;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
uint32_t  MyPageError = 0;
FLASH_EraseInitTypeDef ErasePAGE31Init= {
		.NbPages = 1,
		.Page = 31,
		.TypeErase = FLASH_TYPEERASE_PAGES
};
typedef enum edge_enum {
	None,
	Rise,
	Fall
} edge_typdef;
edge_typdef Edge = None;
typedef enum power_enum {
	Off,
	On
} power_typdef;
power_typdef Power = Off;
typedef enum dimming_direction_enum {
	Down,
	Up
} dimming_direction_typdef;

typedef enum
{
	Proximity,
	Touch
} Software_type;
Software_type Software = Proximity;
dimming_direction_typdef DimmingDirectionCh1 = Up;
dimming_direction_typdef DimmingDirectionCh2 = Up;
uint8_t FallingEdge = 1;
uint8_t RisingEdge = 0;
uint8_t TimeOut = 0;
uint8_t DimmingLevelCh1 = MIN_PWM;
uint8_t DimmingLevelCh2 = MIN_PWM;
uint16_t TargetLevelCh1 = 0;
uint16_t TargetLevelCh2 = 0;
uint16_t mSec = 0;
uint16_t mSec_Allarm = 0;
uint16_t*  FlashPtr = 0;
uint16_t i=0;
uint8_t TxBuffer[3] = {0};
uint8_t RxBuffer[3] = {0};
uint8_t I2C_Trasmit_Buffer[2] = {0};
uint8_t I2C_Recive_Buffer[2] = {0};
uint16_t proximity = 0;
uint16_t threshold = 0;
uint8_t interrupt_status = 0;
uint8_t slave_does_not_respond = 0;
uint8_t is_to_ceck = 0;
HAL_StatusTypeDef recive = 1;
HAL_UART_StateTypeDef state = 0;
uint8_t touch_count = 0;
uint16_t mSecDoubleTouch = 0;
uint8_t Channel = 1;
uint8_t channel_is_changed = 0;
uint16_t mSecTouch = 0;
uint8_t BufferMemCh1 = 0;
uint8_t BufferMemCh2 = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_COMP1_Init(void);
static void MX_I2C2_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM14_Init(void);
static void MX_TIM17_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void HAL_SYSTICK_Callback(void)
{
	if(mSec < 5000) mSec++;

	if(mSecDoubleTouch < MAXIMUM_DOUBLE_TOUCH_TIME) mSecDoubleTouch++;
	if(mSecDoubleTouch == MAXIMUM_DOUBLE_TOUCH_TIME)
	{
		touch_count = 0;
	}

#ifdef SLAVE_RESPONSE
	if(slave_does_not_respond > 1)
	{
		if(mSec_Allarm < 250) mSec_Allarm++;
		if(mSec_Allarm == 250)
		{
			//if((htim2.Instance->CCR3 != 0) && (htim2.Instance->CCR3 != 255)) htim2.Instance->CCR3 = 255;
			//if(htim2.Instance->CCR3 == 0) htim2.Instance->CCR3 = 255;
			//else if(htim2.Instance->CCR3 == 255) htim2.Instance->CCR3 = 0;



			if(htim2.Instance->CCR3 == 0)
			{
				//HAL_GPIO_WritePin(DEBUG_GPIO_Port, DEBUG_Pin, GPIO_PIN_RESET);
				htim17.Instance->CCR1 = 0;
				htim2.Instance->CCR3 = 100;
			}
			else if(htim2.Instance->CCR3 == 100)
			{
				//HAL_GPIO_WritePin(DEBUG_GPIO_Port, DEBUG_Pin, GPIO_PIN_SET);
				htim17.Instance->CCR1 = 100;
				htim2.Instance->CCR3 = 0;
			}
			mSec_Allarm = 0;
		}
	}
#endif
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_COMP1_Init();
  MX_I2C2_Init();
  MX_USART1_UART_Init();
  MX_TIM2_Init();
  MX_TIM14_Init();
  MX_TIM17_Init();
  /* USER CODE BEGIN 2 */
	HAL_Delay(10);

	/* Controllare PWM gen prima di modificare, sia master che slave  750khz, pwm50%*/
	if (HAL_GPIO_ReadPin(MODE_SELECT_GPIO_Port, MODE_SELECT_Pin) == GPIO_PIN_RESET)
	{
		// Per proximity è gia configurato, nella dichiarazione
		Software = Touch;
	}
	//Resistenza di limitazione
	//NON PIU USATA LA CORRENTE DI LIMITAZIONE HAL_GPIO_WritePin(CURRENT_LIMIT_GPIO_Port, CURRENT_LIMIT_Pin, GPIO_PIN_SET);				//Resistenza di limitazione
	HAL_COMP_Start(&hcomp1);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3); // led 3
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);  // led 1
	HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1); // pwm gen out
	HAL_TIM_PWM_Start(&htim17, TIM_CHANNEL_1); // led 2
	htim2.Instance->CCR1 = 0;
	htim2.Instance->CCR3 = 0;
	htim17.Instance->CCR1 = 0;

//#ifdef PROXIMITY
	if (Software == Proximity)
	{
		VCNL4020_Init(&hi2c2, THRESHOLD_LEVEL);
		htim2.Instance->CCR1 = MIN_LED_BLUE_LEVEL; //Led Blue
	}
//#endif

	FlashPtr = (uint16_t*)DIMMING_CH1_FLASH;
	DimmingLevelCh1 = *FlashPtr;
	FlashPtr = (uint16_t*)DIMMING_CH2_FLASH;
	DimmingLevelCh2 = *FlashPtr;
	FlashPtr = (uint16_t*)POWER_FLASH;
	Power = *FlashPtr;
	FlashPtr = (uint16_t*)DIMMINGDIRECTION_CH1_FLASH;
	DimmingDirectionCh1 = *FlashPtr;
	FlashPtr = (uint16_t*)DIMMINGDIRECTION_CH2_FLASH;
	DimmingDirectionCh2 = *FlashPtr;
	FlashPtr = (uint16_t*)CHANNEL_SELECTED_FLASH;
	Channel = *FlashPtr;

	if(Power == On)
	{
		Edge = Rise;
		TargetLevelCh1 = DimmingLevelCh1;
		TargetLevelCh2 = DimmingLevelCh2;
	}
	if((DimmingLevelCh1 == 0xFF)||(DimmingLevelCh2 == 0xFF)||(Power == 0xFF)||(DimmingDirectionCh1 == 0xFF)||(DimmingDirectionCh2 == 0xFF)||(Channel == 0xFF))							//Se la memoria e' vuota
	{
		DimmingDirectionCh1 = Up;
		DimmingDirectionCh2 = Up;
		Power = Off;
		DimmingLevelCh1 = MIN_PWM;
		DimmingLevelCh2 = MIN_PWM;
		Channel = 1;
	}
	HAL_Delay(300);

	// Da commentare dopo
	Channel = 1;
	Power = On;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
/*
	  while(1)
	  {
			HAL_Delay(200);
			TxBuffer[0] = 153;
			TxBuffer[1] = 153;
			TxBuffer[2] = 153;
			HAL_UART_Transmit(&huart1, TxBuffer, 3, 500);
	  }*/
	if((TimeOut)&&(mSecDoubleTouch == MAXIMUM_DOUBLE_TOUCH_TIME)&&(channel_is_changed == 0))
	{
		if((mSecTouch > MINIMUM_SHORT_TOUCH_TIME) && (mSecTouch < MAXIMUM_SHORT_TOUCH_TIME))
		{
			if(Power == Off)
			{
				Power = On;
				TargetLevelCh1 = DimmingLevelCh1;
				TargetLevelCh2 = DimmingLevelCh2;
				Edge = Rise;
				BufferMemCh1 = 0;
				BufferMemCh2 = 0;
			}
			else
			{
				Power = Off;
				TargetLevelCh1 = 0;
				TargetLevelCh2 = 0;
				Edge = Fall;
				BufferMemCh1 = DimmingLevelCh1;
				BufferMemCh2 = DimmingLevelCh2;
			}
			mSec = 0;
			mSecTouch = MAXIMUM_SHORT_TOUCH_TIME + 1;
		}
		HAL_FLASH_Unlock();
		HAL_FLASHEx_Erase(&ErasePAGE31Init, &MyPageError);
		FlashPtr = (uint16_t*)DIMMING_CH1_FLASH;
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, (uint32_t)FlashPtr, (uint8_t)DimmingLevelCh1);
		FlashPtr = (uint16_t*)DIMMING_CH2_FLASH;
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, (uint32_t)FlashPtr, (uint8_t)DimmingLevelCh2);
		FlashPtr = (uint16_t*)POWER_FLASH;
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, (uint32_t)FlashPtr, (uint8_t)Power);
		FlashPtr = (uint16_t*)DIMMINGDIRECTION_CH1_FLASH;
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, (uint32_t)FlashPtr, (uint8_t)DimmingDirectionCh1);
		FlashPtr = (uint16_t*)DIMMINGDIRECTION_CH2_FLASH;
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, (uint32_t)FlashPtr, (uint8_t)DimmingDirectionCh2);
		FlashPtr = (uint16_t*)CHANNEL_SELECTED_FLASH;
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, (uint32_t)FlashPtr, (uint8_t)Channel);
		TimeOut = 0;
	}

	if((Power == On)&&(Edge == None))
	{
		if((RisingEdge)&&(mSec > MAXIMUM_SHORT_TOUCH_TIME))
		{
			HAL_Delay(MANUAL_RAMP_DELAY_TIME);
			if(Channel == 1)
			{
				if(DimmingDirectionCh1 == Up)
				{
					if((DimmingLevelCh1 < MAX_PWM)&&(RisingEdge == 1)) DimmingLevelCh1 ++;			//Controllo se il tasto è ancora premuto
				}
				else
				{
#ifdef ONE_CHANNEL_OFF
					if(DimmingLevelCh2 >= MIN_PWM)
					{

						if((DimmingLevelCh1 > 0)&&(RisingEdge == 1)) DimmingLevelCh1 --;			//Controllo se il tasto è ancora premuto
					}
					else
					{
#endif
						if((DimmingLevelCh1 > MIN_PWM)&&(RisingEdge == 1)) DimmingLevelCh1 --;			//Controllo se il tasto è ancora premuto
#ifdef ONE_CHANNEL_OFF
					}
#endif
				}
			}
			else if(Channel == 2)
			{
				if(DimmingDirectionCh2 == Up)
				{
					if((DimmingLevelCh2 < MAX_PWM)&&(RisingEdge == 1)) DimmingLevelCh2 ++;			//Controllo se il tasto è ancora premuto
				}
				else
				{
#ifdef ONE_CHANNEL_OFF
					if(DimmingLevelCh1 >= MIN_PWM)
					{
						if((DimmingLevelCh2 > 0)&&(RisingEdge == 1)) DimmingLevelCh2 --;			//Controllo se il tasto è ancora premuto
					}
					else
					{
#endif
						if((DimmingLevelCh2 > MIN_PWM)&&(RisingEdge == 1)) DimmingLevelCh2 --;			//Controllo se il tasto è ancora premuto
#ifdef ONE_CHANNEL_OFF
					}
#endif
				}
			}
			// Canale 1 e 2 in sincrono
			else if(Channel == 3)
			{
				if(DimmingDirectionCh2 == Up)
				{
					if((DimmingLevelCh2 < MAX_PWM)&&(RisingEdge == 1))
					{
						DimmingLevelCh2 ++;			//Controllo se il tasto è ancora premuto
						DimmingLevelCh1 ++;
					}
				}
				else
				{
#ifdef ONE_CHANNEL_OFF
					if(DimmingLevelCh1 >= MIN_PWM)
					{
						if((DimmingLevelCh2 > 0)&&(RisingEdge == 1))
						{
							DimmingLevelCh2 --;			//Controllo se il tasto è ancora premuto
							DimmingLevelCh1 --;
						}
					}
					else
					{
#endif
						if((DimmingLevelCh2 > MIN_PWM)&&(RisingEdge == 1))
						{
							DimmingLevelCh2 --;			//Controllo se il tasto è ancora premuto
							DimmingLevelCh1 --;
						}
#ifdef ONE_CHANNEL_OFF
					}
#endif
				}
			}

			TxBuffer[0] = 0;
			TxBuffer[1] = DimmingLevelCh1;
			TxBuffer[2] = DimmingLevelCh2;
			HAL_UART_Transmit(&huart1, TxBuffer, 3, 10);
		}
	}

	if(Edge == Rise)
	{
		i = AUTOMATIC_RAMP_DELAY_TIME;
		while(--i > 1) asm("NOP");

		if(BufferMemCh1 < TargetLevelCh1) BufferMemCh1 ++;
		if(BufferMemCh2 < TargetLevelCh2) BufferMemCh2 ++;
		if((BufferMemCh1 == TargetLevelCh1)&&(BufferMemCh2 == TargetLevelCh2)) Edge = None;

		TxBuffer[0] = 0;
		TxBuffer[1] = BufferMemCh1;
		TxBuffer[2] = BufferMemCh2;
		HAL_UART_Transmit(&huart1, TxBuffer, 3, 10);
	}

	if(Edge == Fall)
	{
		i = AUTOMATIC_RAMP_DELAY_TIME;
		while(--i > 1) asm("NOP");

		if(BufferMemCh1 > BufferMemCh2)
		{
			if(BufferMemCh1 > TargetLevelCh1) BufferMemCh1 --;
		}
		else if(BufferMemCh1 < BufferMemCh2)
		{
			if(BufferMemCh2 > TargetLevelCh2) BufferMemCh2 --;
		}
		else if(BufferMemCh1 == BufferMemCh2)
		{
			if(BufferMemCh1 > TargetLevelCh1) BufferMemCh1 --;
			if(BufferMemCh2 > TargetLevelCh2) BufferMemCh2 --;
		}
		if((BufferMemCh1 == TargetLevelCh1)&&(BufferMemCh2 == TargetLevelCh2)) Edge = None;

		TxBuffer[0] = 0;
		TxBuffer[1] = BufferMemCh1;
		TxBuffer[2] = BufferMemCh2;
		HAL_UART_Transmit(&huart1, TxBuffer, 3, 10);
	}

	if((is_to_ceck == 1) && (Edge == None) && (mSec > 1000))
	{
		HAL_Delay(1);
		TxBuffer[0] = 1;
		TxBuffer[1] = 254;
		TxBuffer[2] = 254;
		HAL_UART_Transmit(&huart1, TxBuffer, 3, 500);
		HAL_Delay(1);														//Importante al fine di recepire bene il pacchetto
		HAL_UART_Abort(&huart1);											//Importante al fine di recepire bene il pacchetto
		if(HAL_UART_Receive(&huart1, RxBuffer, 3, 5) == HAL_OK)
		{
			if((RxBuffer[0] == 1)&&(RxBuffer[1] == 255)&&(RxBuffer[2] == 255)) slave_does_not_respond = 0;
			else slave_does_not_respond ++;
		}
		else slave_does_not_respond ++;
		RxBuffer[0] = 0;
		RxBuffer[1] = 0;
		RxBuffer[2] = 0;
		is_to_ceck = 0;
	}

#ifdef SLAVE_RESPONSE
	if(slave_does_not_respond == 0)
	{
#endif
//			if(TxBuffer[1] > 0) htim2.Instance->CCR2 = TxBuffer[1] + 20;
//			else htim2.Instance->CCR2 = 0;
		if(Power == On)
		{
			switch (Channel)
			{
				case 1:
					htim17.Instance->CCR1 = 100;
					htim2.Instance->CCR3 = 0;
					//HAL_GPIO_WritePin(DEBUG_GPIO_Port, DEBUG_Pin, GPIO_PIN_RESET);
					break;
				case 2:
					htim17.Instance->CCR1 = 0;
					htim2.Instance->CCR3 = 100;
					//HAL_GPIO_WritePin(DEBUG_GPIO_Port, DEBUG_Pin, GPIO_PIN_SET);
					break;
				case 3:
					htim17.Instance->CCR1 = 100;
					htim2.Instance->CCR3 = 100;
					//HAL_GPIO_WritePin(DEBUG_GPIO_Port, DEBUG_Pin, GPIO_PIN_SET);
					break;
				default:
					break;
			}
		}
		else if(Edge == None)
		{
			htim17.Instance->CCR1 = 0;
			htim2.Instance->CCR3 = 0;
			//HAL_GPIO_WritePin(DEBUG_GPIO_Port, DEBUG_Pin, GPIO_PIN_RESET);
		}
#ifdef SLAVE_RESPONSE
	}
#endif
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the peripherals clocks
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief COMP1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_COMP1_Init(void)
{

  /* USER CODE BEGIN COMP1_Init 0 */

  /* USER CODE END COMP1_Init 0 */

  /* USER CODE BEGIN COMP1_Init 1 */

  /* USER CODE END COMP1_Init 1 */
  hcomp1.Instance = COMP1;
  hcomp1.Init.InputPlus = COMP_INPUT_PLUS_IO3;
  hcomp1.Init.InputMinus = COMP_INPUT_MINUS_1_4VREFINT;
  hcomp1.Init.OutputPol = COMP_OUTPUTPOL_INVERTED;
  hcomp1.Init.WindowOutput = COMP_WINDOWOUTPUT_EACH_COMP;
  hcomp1.Init.Hysteresis = COMP_HYSTERESIS_NONE;
  hcomp1.Init.BlankingSrce = COMP_BLANKINGSRC_NONE;
  hcomp1.Init.Mode = COMP_POWERMODE_HIGHSPEED;
  hcomp1.Init.WindowMode = COMP_WINDOWMODE_DISABLE;
  hcomp1.Init.TriggerMode = COMP_TRIGGERMODE_NONE;
  if (HAL_COMP_Init(&hcomp1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN COMP1_Init 2 */

  /* USER CODE END COMP1_Init 2 */

}

/**
  * @brief I2C2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C2_Init(void)
{

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.Timing = 0x00100413;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /** I2C Fast mode Plus enable
  */
  HAL_I2CEx_EnableFastModePlus(I2C_FASTMODEPLUS_I2C2);
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 63;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 252;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM14 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM14_Init(void)
{

  /* USER CODE BEGIN TIM14_Init 0 */

  /* USER CODE END TIM14_Init 0 */

  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM14_Init 1 */

  /* USER CODE END TIM14_Init 1 */
  htim14.Instance = TIM14;
  htim14.Init.Prescaler = 10;
  htim14.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim14.Init.Period = 3;
  htim14.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim14.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim14) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim14) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 2;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim14, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM14_Init 2 */

  /* USER CODE END TIM14_Init 2 */
  HAL_TIM_MspPostInit(&htim14);

}

/**
  * @brief TIM17 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM17_Init(void)
{

  /* USER CODE BEGIN TIM17_Init 0 */

  /* USER CODE END TIM17_Init 0 */

  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM17_Init 1 */

  /* USER CODE END TIM17_Init 1 */
  htim17.Instance = TIM17;
  htim17.Init.Prescaler = 63;
  htim17.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim17.Init.Period = 252;
  htim17.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim17.Init.RepetitionCounter = 0;
  htim17.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim17) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim17) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim17, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim17, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM17_Init 2 */

  /* USER CODE END TIM17_Init 2 */
  HAL_TIM_MspPostInit(&htim17);

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart1, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart1, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin : MODE_SELECT_Pin */
  GPIO_InitStruct.Pin = MODE_SELECT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(MODE_SELECT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PROXIMITY_Pin */
  GPIO_InitStruct.Pin = PROXIMITY_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(PROXIMITY_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : TOUCH_Pin */
  GPIO_InitStruct.Pin = TOUCH_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(TOUCH_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI2_3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI2_3_IRQn);

  HAL_NVIC_SetPriority(EXTI4_15_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);

}

/* USER CODE BEGIN 4 */

void HAL_GPIO_EXTI_Rising_Callback(uint16_t GPIO_Pin)
{
//#ifdef TOUCH
	if (Software == Touch)
	{
	if(GPIO_Pin == TOUCH_Pin)
	{
		if(FallingEdge)
		{
			FallingEdge = 0;
			RisingEdge = 1;
			mSec = 0;
			TimeOut = 0;
			channel_is_changed = 0;
		}
	}
	}
//#endif
}
void HAL_GPIO_EXTI_Falling_Callback(uint16_t GPIO_Pin)
{
//#ifdef PROXIMITY
if (Software == Proximity)
{
	if(GPIO_Pin == PROXIMITY_Pin)
	{
		if(I2C_Read_Byte(&hi2c2, VCNL4020_ADDRESS, VCNL4020_INTERRUPT_STATUS_REG) == 0x01)	//Se e' stata superata la soglia alta
		{
			I2C_Write_Byte(&hi2c2, VCNL4020_ADDRESS, VCNL4020_INTERRUPT_STATUS_REG, 0x01);  //Clear flag

			VCNL4020_Set_Threshold(&hi2c2, THRESHOLD_LEVEL - 1000, 0xFFFF);

			I2C_Write_Byte(&hi2c2, VCNL4020_ADDRESS, VCNL4020_COMMAND_REG, 0x03);

			if(FallingEdge)
			{
				FallingEdge = 0;
				RisingEdge = 1;
				mSec = 0;
				TimeOut = 0;
				channel_is_changed = 0;
			}
			htim2.Instance->CCR1 = 254;														//Led Blue
		}
		if(I2C_Read_Byte(&hi2c2, VCNL4020_ADDRESS, VCNL4020_INTERRUPT_STATUS_REG) == 0x02)	//Se e' stata superata la soglia bassa
		{
			I2C_Write_Byte(&hi2c2, VCNL4020_ADDRESS, VCNL4020_INTERRUPT_STATUS_REG, 0x02);  //Clear flag

			VCNL4020_Set_Threshold(&hi2c2, 0, THRESHOLD_LEVEL);

			I2C_Write_Byte(&hi2c2, VCNL4020_ADDRESS, VCNL4020_COMMAND_REG, 0x03);

			if(RisingEdge)
			{
				FallingEdge = 1;
				RisingEdge = 0;
				TimeOut = 1;
				is_to_ceck = 1;
				if((mSec > MAXIMUM_SHORT_TOUCH_TIME)&&(Power == On))
				{
					if(Channel == 1)
					{
						if(DimmingDirectionCh1 == Up) DimmingDirectionCh1 = Down;
						else DimmingDirectionCh1 = Up;
					}
					else if(Channel == 2)
					{
						if(DimmingDirectionCh2 == Up) DimmingDirectionCh2 = Down;
						else DimmingDirectionCh2 = Up;
					}
					else if(Channel == 3)
					{
						if(DimmingDirectionCh2 == Up) DimmingDirectionCh2 = Down;
						else DimmingDirectionCh2 = Up;
					}
				}
				if(mSec < MAXIMUM_SHORT_TOUCH_TIME)
				{
					if(touch_count == 0)
					{
						touch_count = 1;
						mSecDoubleTouch = 0;
						mSecTouch = mSec;
					}
					else if((touch_count == 1)&&(mSecDoubleTouch < MAXIMUM_DOUBLE_TOUCH_TIME))
					{
						if(Power == On)
						{
							/*	if(Channel == 1) Channel = 2;
							else if(Channel == 2) Channel = 1;*/
							Channel ++;
							// Channel = 3 son due canali in sincrono
							if (Channel == 3)
							{
								if (DimmingLevelCh2 == DimmingLevelCh1)
								{
									DimmingDirectionCh1 = Up;
									DimmingDirectionCh2 = Up;
								}
								else
								{
									DimmingDirectionCh1 = Up;
									DimmingDirectionCh2 = Up;

									// Ch1 = 70 Ch2 = 90
									uint16_t half_dimming_level = (DimmingLevelCh2 + DimmingLevelCh1) / 2;
									if (DimmingLevelCh1 < DimmingLevelCh2)
									{
										for (; DimmingLevelCh1 <= half_dimming_level; DimmingLevelCh1 ++)
										{
											i = AUTOMATIC_RAMP_DELAY_TIME;
											while(--i > 1) asm("NOP");

											TxBuffer[0] = 0;
											TxBuffer[1] = DimmingLevelCh1;
											TxBuffer[2] = DimmingLevelCh2;
											HAL_UART_Transmit(&huart1, TxBuffer, 3, 10);
											DimmingLevelCh2 --;
										}
									}
									else
									{
										for (; DimmingLevelCh2 <= half_dimming_level; DimmingLevelCh2 ++)
										{
											i = AUTOMATIC_RAMP_DELAY_TIME;
											while(--i > 1) asm("NOP");

											TxBuffer[0] = 0;
											TxBuffer[1] = DimmingLevelCh1;
											TxBuffer[2] = DimmingLevelCh2;
											HAL_UART_Transmit(&huart1, TxBuffer, 3, 10);
											DimmingLevelCh1 --;
										}
									}

									TxBuffer[0] = 0;
									TxBuffer[1] = half_dimming_level;
									TxBuffer[2] = half_dimming_level;

									DimmingLevelCh1 = half_dimming_level;
									DimmingLevelCh2 = DimmingLevelCh1;
									HAL_UART_Transmit(&huart1, TxBuffer, 3, 10);
									 asm("NOP");
								}
							}
							if (Channel > 3) Channel = 1;

						}
						touch_count = 0;
						channel_is_changed = 1;
						mSecTouch = MAXIMUM_SHORT_TOUCH_TIME + 1;
					}
				}
			}
			htim2.Instance->CCR1 = MIN_LED_BLUE_LEVEL;										//Led Blue
		}
	}
//#endif
}
//#ifdef TOUCH
else
{
if(GPIO_Pin == TOUCH_Pin)
	{
		if(RisingEdge)
		{
			FallingEdge = 1;
			RisingEdge = 0;
			TimeOut = 1;
			is_to_ceck = 1;
			if((mSec > MAXIMUM_SHORT_TOUCH_TIME)&&(Power == On))
			{
				if(Channel == 1)
				{
					if(DimmingDirectionCh1 == Up) DimmingDirectionCh1 = Down;
					else DimmingDirectionCh1 = Up;
				}
				else if(Channel == 2)
				{
					if(DimmingDirectionCh2 == Up) DimmingDirectionCh2 = Down;
					else DimmingDirectionCh2 = Up;
				}
				else if(Channel == 3)
				{
					if(DimmingDirectionCh2 == Up) DimmingDirectionCh2 = Down;
					else DimmingDirectionCh2 = Up;
				}
			}
			if(mSec < MAXIMUM_SHORT_TOUCH_TIME)
			{
				if(touch_count == 0)
				{
					touch_count = 1;
					mSecDoubleTouch = 0;
					mSecTouch = mSec;
				}
				else if((touch_count == 1)&&(mSecDoubleTouch < MAXIMUM_DOUBLE_TOUCH_TIME))
				{
					if(Power == On)
					{

						/*	if(Channel == 1) Channel = 2;
						else if(Channel == 2) Channel = 1;*/
						Channel ++;
						// Channel = 3 son due canali in sincrono
						if (Channel == 3)
						{
							if (DimmingLevelCh2 == DimmingLevelCh1)
							{
								DimmingDirectionCh1 = Up;
								DimmingDirectionCh2 = Up;
							}
							else
							{
								DimmingDirectionCh1 = Up;
								DimmingDirectionCh2 = Up;

								// Ch1 = 70 Ch2 = 90
								uint16_t half_dimming_level = (DimmingLevelCh2 + DimmingLevelCh1) / 2;
								if (DimmingLevelCh1 < DimmingLevelCh2)
								{
									for (; DimmingLevelCh1 <= half_dimming_level; DimmingLevelCh1 ++)
									{
										i = AUTOMATIC_RAMP_DELAY_TIME;
										while(--i > 1) asm("NOP");

										TxBuffer[0] = 0;
										TxBuffer[1] = DimmingLevelCh1;
										TxBuffer[2] = DimmingLevelCh2;
										HAL_UART_Transmit(&huart1, TxBuffer, 3, 10);
										DimmingLevelCh2 --;
									}
								}
								else
								{
									for (; DimmingLevelCh2 <= half_dimming_level; DimmingLevelCh2 ++)
									{
										i = AUTOMATIC_RAMP_DELAY_TIME;
										while(--i > 1) asm("NOP");

										TxBuffer[0] = 0;
										TxBuffer[1] = DimmingLevelCh1;
										TxBuffer[2] = DimmingLevelCh2;
										HAL_UART_Transmit(&huart1, TxBuffer, 3, 10);
										DimmingLevelCh1 --;
									}
								}

								TxBuffer[0] = 0;
								TxBuffer[1] = half_dimming_level;
								TxBuffer[2] = half_dimming_level;

								DimmingLevelCh1 = half_dimming_level;
								DimmingLevelCh2 = DimmingLevelCh1;
								HAL_UART_Transmit(&huart1, TxBuffer, 3, 10);
								 asm("NOP");
							}
						}
						if (Channel > 3) Channel = 1;
					}
					touch_count = 0;
					channel_is_changed = 1;
					mSecTouch = MAXIMUM_SHORT_TOUCH_TIME + 1;
				}
			}
		}
	}
//#endif
}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
